from aiohttp import web
from aiohttp_apispec import docs, response_schema
from api import schema


class HowToFibo(web.View):
    @staticmethod
    def fibonacci(n):
        fib1 = fib2 = 1
        fib = [fib1, fib2]
        while fib2 < n:
            fib1, fib2 = fib2, fib1 + fib2
            if fib2 < n:
                fib.append(fib2)
        return fib

    @docs(
        tags=["HowToFibo"],
        summary="Display Fibonacci numbers that less that value from URL",
        description="Post value from url and display Fibonacci numbers that less that value in response",
    )
    @response_schema(
        schema=schema.SuccessSchema(),
        code=200,
        description="Return success response with dict",
    )
    @response_schema(
        schema=schema.BadRequestSchema(),
        code=400,
        description="Return success response with dict",
    )
    async def post(self):
        try:
            value = int(self.request.match_info.get("value"))
        except Exception:
            raise web.HTTPBadRequest(reason="The value is not int.")
        if value >= 1000000:
            raise web.HTTPBadRequest(reason="The value is greater than the allowable.")
        elif value <= 0:
            raise web.HTTPBadRequest(reason="The value is equal or less than zero.")
        return web.json_response({"result": self.fibonacci(value)})

from aiohttp import web
from aiohttp_apispec import docs, response_schema
from api import schema


class LetsDict(web.View):

    def take(self, total_int=0, n=0, total_decimal=0):
        data = dict()
        data["count"] = self.request.rel_url.query.__len__()
        for key, value in self.request.rel_url.query.items():
            n += 1
            if n % 2 == 0:
                try:
                    data[key] = int(value)
                    total_int += int(value)
                except Exception:
                    data[key] = value
            else:
                try:
                    data[key] = float(value)
                    total_decimal += float(value)
                except Exception:
                    data[key] = value[::-1]
        data["total_int"] = total_int
        data["total_decimal"] = total_decimal
        return data

    @docs(
        tags=["LetsDict"],
        summary="Display the next dict from URL",
        description="Put the next dict from url and display in response",
    )
    @response_schema(
        schema=schema.SuccessSchema(),
        code=200,
        description="Return success response with dict",
    )
    @response_schema(
        schema=schema.BadRequestSchema(),
        code=400,
        description="Return success response with dict",
    )
    async def put(self):
        return web.json_response({"result": self.take()})
